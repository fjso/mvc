package struts.form;

import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class ActionListener implements ServletContextListener {

	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		// TODO Auto-generated method stub
         System.out.println("系统已经注销!");
        
	}

	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		// TODO Auto-generated method stub
		ServletContext context = arg0.getServletContext();
        //超前加载
		//得到xml配置文件地址
        String xmlpath = context.getInitParameter("struts-config");
        //得到tomcat路径
        String tomcatpath = context.getRealPath("\\");
       
        //通过全路径找到Struts-config文件,然后解析
        try {
			Map<String,XmlBean> map = Struts_xml.resolveXml(tomcatpath+xmlpath);
			context.setAttribute("struts",map);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        System.out.println("信息系统xml已经加载完成！");
        System.out.println("============"+tomcatpath+","+xmlpath);
	}

}
