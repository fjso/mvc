package struts.form;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;

public class Struts_xml {

	   public Struts_xml(){
		   
	   }
	   
	   public static Map<String,XmlBean> resolveXml(String xmlpath) throws Exception{
//		       路径常量:"bin/struts-config.xml"
		    /*1.定义一个SAXBuilder对象*/
			SAXBuilder builder = new SAXBuilder();
			Document document = builder.build(new File(xmlpath));
			Element element = document.getRootElement();
			/*定义一个存放结果的Map对象*/
			Map<String,XmlBean> rmap = new HashMap<String,XmlBean>();
			Element actionmapping = element.getChild("action-mapping");
			List<Element>  actionmappingdown = actionmapping.getChildren();
		
			for(Element e1:actionmappingdown){
				XmlBean action = new XmlBean();
				String name = e1.getAttributeValue("name");
				action.setBeanName(name);
				Element actionform = element.getChild("formbeans");
				List<Element> form = actionform.getChildren();
				for (Element e:form){
					 if (e.getAttributeValue("name").equals(name))
					 {
						 String formclass = e.getAttributeValue("class");
						 action.setFormClass(formclass);
						 break;
					 }  
			    }
				String path = e1.getAttributeValue("path");
				action.setPath(path);
				String type = e1.getAttributeValue("type");
				action.setActionClass(type);
				List<Element> x = e1.getChildren();
				Map<String,String> map = new HashMap<String,String>();
				for(Element xx:x){
					String name1 = xx.getAttributeValue("name");
					String value = xx.getAttributeValue("value");
					map.put(name1, value);
					//System.out.println("name="+name1+"||"+value);
					
				}
				action.setActionForward(map);
				rmap.put(path,action);
//				System.out.println("==========================================================");
//				System.out.println("name="+name+"||path="+path+"||type="+type);
			}
			return rmap;
		}
}
