package bisness;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import services.myService.panduan.PanduanServiceImpl;
import services.vo.MessageVO;
import struts.action.Action;
import struts.form.ActionForm;

public class PandAction implements Action {

	public PandAction(){
		
	}
	@Override
	public String execute(HttpServletRequest request,HttpServletResponse response,ActionForm form,Map<String,String> actionForward) {
		// TODO Auto-generated method stub
        String url ="";
        PanduanServiceImpl vo =new PanduanServiceImpl();
		Map<String,MessageVO> map= vo.getVO(request.getParameter("text"));
		Set<String> set = map.keySet();
		for (Iterator<String> it=set.iterator();it.hasNext();){
			url=it.next();
			request.setAttribute("vo",map.get(url));
		}
		return url;
	}

}
